{*
{$field_value|date_format:$TIME_FORMAT|pretty_date|rus_date}
{$field_value|date_format:$DATE_FORMAT|pretty_date|rus_date}
{$field_value|date_format:$TIME_FORMAT|utf8|rus_date}
{$field_value|date_format:$DATE_FORMAT|utf8|rus_date}
Семантически корректный пример вывода даты в паблик: <time datetime="{$field_value|date_format:'%Y-%m-%d'}">{$field_value|date_format:$DATE_FORMAT|pretty_date|rus_date}</time>
*}
{$field_value|date_format:'%d-%m-%Y'}